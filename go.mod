module gitlab.com/jscarter3/aws-session-manager

require (
	github.com/aws/aws-sdk-go v1.15.2
	github.com/davecgh/go-spew v1.1.0 // indirect
	github.com/go-ini/ini v1.38.1
	github.com/gopherjs/gopherjs v0.0.0-20180628210949-0892b62f0d9f // indirect
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/jmespath/go-jmespath v0.0.0-20180206201540-c2b33e8439af // indirect
	github.com/jtolds/gls v4.2.1+incompatible // indirect
	github.com/pkg/errors v0.8.0
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/smartystreets/assertions v0.0.0-20180803164922-886ec427f6b9 // indirect
	github.com/smartystreets/goconvey v0.0.0-20180222194500-ef6db91d284a // indirect
	github.com/spf13/cobra v0.0.3
	github.com/spf13/pflag v1.0.1 // indirect
	github.com/stretchr/testify v1.2.2 // indirect
	golang.org/x/net v0.0.0-20180807145015-19491d39cadb // indirect
	golang.org/x/text v0.3.0 // indirect
	gopkg.in/ini.v1 v1.38.1 // indirect
)
