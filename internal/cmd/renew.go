package cmd

import (
	"log"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/iam"
	"github.com/aws/aws-sdk-go/service/sts"
	"github.com/spf13/cobra"
)

var renewCmd = &cobra.Command{
	Use:   "renew <token>",
	Short: "renew session for profile",
	Run:   runRenew,
	Args:  cobra.ExactArgs(1),
}

func runRenew(cmd *cobra.Command, args []string) {
	if len(args) < 1 {
		log.Fatalf("No token given, unable to renew session")
	}
	creds, err := config.getMasterCredential()
	if err != nil {
		log.Fatalf("Unable to load master credentials\n%v", err)
	}
	sess := session.Must(session.NewSession(&aws.Config{
		Credentials: creds,
	}))
	iams := iam.New(sess)
	mfaOutput, err := iams.ListMFADevices(&iam.ListMFADevicesInput{})
	if err != nil {
		log.Fatalf("Unable to list MFA devices: %v", err)
	}
	if len(mfaOutput.MFADevices) < 1 {
		log.Fatalf("No MFA devices found")
	}

	stss := sts.New(sess)
	tokenOutput, err := stss.GetSessionToken(&sts.GetSessionTokenInput{
		DurationSeconds: aws.Int64(28800),
		SerialNumber:    mfaOutput.MFADevices[0].SerialNumber,
		TokenCode:       &args[0],
	})
	if err != nil {
		log.Fatalf("Unable to retrieve session token: %v", err)
	}
	err = config.updateGeneratedCredentials(*tokenOutput.Credentials)
	if err != nil {
		log.Fatalf("failed to update token: %v\n", err)
	}
	err = config.save()
	if err != nil {
		log.Fatalf("failed to update token: %v\n", err)
	}
	log.Printf("Successfully renewed token for profile %s\n", config.Profile)
}
