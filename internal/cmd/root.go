package cmd

import (
	"os"
	"runtime"

	"fmt"

	"time"

	"path/filepath"

	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/service/sts"
	"github.com/go-ini/ini"
	"github.com/pkg/errors"
	"github.com/spf13/cobra"
)

var AWSMCmd = &cobra.Command{
	Use:   "awsm",
	Short: "tool to help manage 2fa",
}

type awsmData struct {
	master    *ini.File
	generated *ini.File
	Profile   string
}

var config awsmData

func Execute() {
	config.load()
	AWSMCmd.Version = "0.1.0"
	AWSMCmd.AddCommand(renewCmd)
	AWSMCmd.AddCommand(validateCmd)
	AWSMCmd.AddCommand(whoCmd)

	AWSMCmd.PersistentFlags().StringVarP(&config.Profile, "profile", "p", "", "profile to use")
	AWSMCmd.PersistentPreRun = defaultProfile

	if _, err := AWSMCmd.ExecuteC(); err != nil {
		fmt.Printf("Error: %v\n", err)
		os.Exit(-1)
	}
}

func (awsm *awsmData) getMasterCredential() (*credentials.Credentials, error) {
	if awsm.master == nil {
		awsm.load()
	}
	section, err := awsm.master.GetSection(awsm.Profile)
	if err != nil {
		return nil, errors.Wrap(err, "Unable to load profile")
	}
	accessKey := section.Key("aws_access_key_id").MustString("")
	secretKey := section.Key("aws_secret_access_key").MustString("")
	//fmt.Printf("%s, %s\n", accessKey, secretKey)
	return credentials.NewStaticCredentials(accessKey, secretKey, ""), nil
}

func (awsm *awsmData) getGeneratedCredential() (*credentials.Credentials, error) {
	section, err := awsm.generated.GetSection(awsm.Profile)
	if err != nil {
		return nil, errors.Wrap(err, "Unable to load profile")
	}
	accessKey := section.Key("aws_access_key_id").MustString("")
	secretKey := section.Key("aws_secret_access_key").MustString("")
	sessionToken := section.Key("aws_session_token").MustString("")
	if section.HasKey("expiration") {
		t, err := section.Key("expiration").Time()
		if err != nil {
			return nil, errors.Wrap(err, "error parsting expiration")
		}
		if time.Now().After(t) {
			return nil, errors.New("credential is expired")
		}
	}
	//fmt.Printf("%s, %s, %s\n", accessKey, secretKey, sessionToken)
	return credentials.NewStaticCredentials(accessKey, secretKey, sessionToken), nil
}

func (awsm *awsmData) updateGeneratedCredentials(creds sts.Credentials) error {
	section, err := awsm.generated.NewSection(awsm.Profile)
	if err != nil {
		return errors.Wrap(err, "Unable to load profile")
	}
	_, err = section.NewKey("aws_access_key_id", *creds.AccessKeyId)
	if err != nil {
		return errors.Wrap(err, "unable to set access key")
	}
	_, err = section.NewKey("aws_secret_access_key", *creds.SecretAccessKey)
	if err != nil {
		return errors.Wrap(err, "unable to set secret key")
	}
	_, err = section.NewKey("aws_session_token", *creds.SessionToken)
	if err != nil {
		return errors.Wrap(err, "unable to set token")
	}
	t, _ := creds.Expiration.MarshalText()
	_, err = section.NewKey("expiration", string(t))
	if err != nil {
		return errors.Wrap(err, "unable to set expiration")
	}
	return nil
}

func (awsm *awsmData) load() error {
	//cfg, err := ini.Load("/home/jcarter/.aws/credentials")
	master, err := ini.Load(filepath.Join(userHome(), ".aws", "credentials_master"))
	if err != nil {
		return errors.Wrap(err, "unable to open master credentials")
	}
	awsm.master = master
	generated, err := ini.Load(filepath.Join(userHome(), ".aws", "credentials"))
	if err != nil {
		return errors.Wrap(err, "unable to open temp credentials")
	}
	awsm.generated = generated
	return nil
}

func userHome() string {
	if runtime.GOOS == "windows" { // Windows
		return os.Getenv("USERPROFILE")
	}

	// *nix
	return os.Getenv("HOME")
}

func (awsm *awsmData) save() error {
	return awsm.generated.SaveTo(filepath.Join(userHome(), ".aws", "credentials"))
}

func defaultProfile(cmd *cobra.Command, args []string) {
	if config.Profile == "" {
		config.Profile = os.Getenv("AWS_PROFILE")
	}
	if config.Profile == "" {
		config.Profile = "default"
	}
}
