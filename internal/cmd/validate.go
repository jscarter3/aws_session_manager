package cmd

import (
	"log"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/iam"
	"github.com/spf13/cobra"
)

var validateCmd = &cobra.Command{
	Use:   "validate",
	Short: "check if current credentials/session are valid",
	Run:   runValidate,
}

func runValidate(cmd *cobra.Command, args []string) {
	//u, _ := user.Current()
	//u.HomeDir()
	creds, err := config.getGeneratedCredential()
	if err != nil {
		log.Fatalf("Unable to generate credentials\n%v", err)
	}
	sess := session.Must(session.NewSession(&aws.Config{
		Credentials: creds,
	}))
	iams := iam.New(sess)
	_, err = iams.GetUser(&iam.GetUserInput{})
	if err != nil {
		log.Fatalf("Invalid credentials: %v", err)
	} else {
		log.Printf("Credentials are valid")
	}
}
