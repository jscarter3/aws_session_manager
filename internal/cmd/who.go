package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

var whoCmd = &cobra.Command{
	Use:   "who",
	Short: "check what profile is currently set",
	Run:   runWho,
}

func runWho(cmd *cobra.Command, args []string) {
	fmt.Printf("AWS Profile: %s\n", config.Profile)
	//creds, err := config.getCredential()
	//if err != nil {
	//	log.Fatalf("Unable to generate credentials\n%v", err)
	//}
	//sess := session.Must(session.NewSession(&aws.Config{
	//	Credentials: creds,
	//}))
	//stss := sts.New(sess)
	//out, err := stss.GetCallerIdentity(&sts.GetCallerIdentityInput{})
	//if err != nil {
	//	log.Fatalf("Failed to get caller identity: %v", err)
	//}
	//log.Printf("%+v\n", out)
	//iams := iam.New(sess)
	//out2, err := iams.GetAccountSummary(&iam.GetAccountSummaryInput{})
	//
	//fmt.Printf("%+v, %v\n", out2, err)
}
