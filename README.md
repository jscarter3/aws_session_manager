# awsm

awsm is a utility to help manage aws 2fa sessions

## Installation
Download from [releases](https://gitlab.com/jscarter3/aws_session_manager/releases) 

## Setup
When using session tokens with AWS, temporary access & secret keys are also used. So to make this
work globally, awsm works off of a master credential file and it generates the standard aws
credential file off of it when renewing sessions. To set this up, copy your existing $HOME/.aws/credentials
file to $HOME/.aws/credentials_master.

## Usage

```sh
Usage:
  awsm [command]

Available Commands:
  help        Help about any command
  renew       renew session for profile
  validate    check if current credentials/session are valid
  who         check what profile is currently set

Flags:
  -h, --help             help for awsm
  -p, --profile string   profile to use

Use "awsm [command] --help" for more information about a command.
```

awsm will use the AWS account specified via the default credential provider chain 
if one is not specified as a configuration.
See [SDK documentation](http://docs.aws.amazon.com/sdk-for-go/api/) for more details.

### Renew Command
Renew aws credential session

```
Usage:
  awsm renew <token> [flags]

Global Flags:
  -p, --profile string   profile to use
```

### Validate Command
Check if current session is valid

```
Usage:
  awsm validate [flags]

Global Flags:
  -p, --profile string   profile to use
```

### Who Command
Returns which profile is used

```
Usage:
  awsm who
```